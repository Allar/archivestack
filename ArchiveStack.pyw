#!/usr/bin/python
#coding=utf-8
import os, copy, DatabaseHandler, GUI

class Button(dict):
    def __init__(self):
        super().__init__()

    def clear(self, layout):
        for buttonList in self:
            for button in self[buttonList]:
                window.removeButton(layout, button)
        super().clear()

    def remove(self, layout, cardID, button):
        self[cardID].remove(button)
        window.removeButton(layout, button)

    def add(self, layout, key, cardID, *refresh):
        button = window.addButton(layout, data[cardID])
        if not refresh:
            if key not in self:
                self[key] = []
            self[key].append(button)
        bgColor = database.colorConverter(data[cardID]['color_identity'])
        button.setStyleSheet(f"background-color: {bgColor}")
        return button

class ListButton(Button):
    def __init__(self):
        super().__init__()
        self.count = {}

    def clear(self):
        self.count.clear()
        super().clear('list')

    def remove(self, cardID, button):
        super().remove('list', cardID, button)

    def add(self, cardID):
        name = data[cardID]['name']
        count = data[cardID]['count']
        cardSet = data[cardID]['card_set']
        if name not in self.count:
            button = super().add('list', name, cardID)
            listButtonClicked(cardID, button)
            self.count[name] = {cardSet: {'card_id': cardID, 'count': count}}
            return button
        else:
            self.count[name][cardSet] = {'card_id': cardID, 'count': count}
            return self[name][0]

    def getCount(self, name):
        finalCount = copy.deepcopy(self.count[name])
        finalCountInt = 0
        deckCount = {}
        for deck in deckData:
            for cardID in deckData[deck]:
                if cardID == '':
                    break
                if name == data[cardID]['name']:
                    cardSet = data[cardID]['card_set']
                    if cardSet not in deckCount:
                        deckCount[cardSet] = 1
                    else:
                        deckCount[cardSet] += 1
        for cardSet in finalCount:
            if cardSet in deckCount:
                finalCount[cardSet]['count'] -= deckCount[cardSet]
            finalCountInt += finalCount[cardSet]['count']
        return finalCount, finalCountInt

    def enableCheckAll(self):
        for name in self:
            self.enableCheck(name)

    def enableCheck(self, name):
        enabled = bool()
        cardSets, count = self.getCount(name)
        enabled = count > 0
        self[name][0].setEnabled(enabled)
        return cardSets, count

    def setToolTip(self, name):
        for cardID in data:
            if data[cardID]['name'] == name:
                setCount, totalCount = self.getCount(name)
                self[name][0].setToolTip(database.toolTip(data[cardID], [setCount, totalCount]))
                return

class DeckButton(Button):
    def __init__(self):
        super().__init__()

    def clear(self):
        super().clear('deck')

    def remove(self, cardID, button):
        super().remove('deck', cardID, button)

    def add(self, cardID, *refresh):
        if refresh:
            button = super().add('deck', cardID, cardID, refresh)
        else:
            button = super().add('deck', cardID, cardID)
        button.setToolTip(database.toolTip(data[cardID], ''))
        deckButtonClicked(cardID, button)
        return button

    def getCount(self):
        cards = {}
        for cardID in self:
            cards[cardID] = len(self[cardID])
        return cards

    def getString(self):
        cardString = ''
        cards = self.getCount()
        for cardID in cards:
            cardString += (cards[cardID]*(cardID + '|'))
        return cardString.rstrip('|')

path = os.path.dirname(os.path.realpath(__file__))
directory = 'bin'
inputPath = os.path.join(path, directory, 'input.txt')
outputPath = os.path.join(path, directory, 'output.txt')
removePath = os.path.join(path, directory, 'remove.txt')

def deckButtonClicked(cardID, button):
    button.clicked.connect(lambda: fromDeck(cardID, button))

def listButtonClicked(cardID, button):
    button.clicked.connect(lambda: toDeck(cardID))

def toDeck(cardID):
    name = data[cardID]['name']
    cardSets, count = listButtons.getCount(name)
    setKeys = list(cardSets)
    setKeys.reverse()
    for cardSet in setKeys:
        count = cardSets[cardSet]['count']
        if count > 0:
            cardID = cardSets[cardSet]['card_id']
            break
    if selectedDeck not in deckData:
        deckData[selectedDeck] = []
    deckData[selectedDeck].append(cardID)
    deckButtons.add(cardID)
    listButtons.enableCheck(name)
    listButtons.setToolTip(name)

def fromDeck(cardID, button):
    name = data[cardID]['name']
    selectedLayout = window.layoutSelectionList.currentData()
    columnName = ''
    if selectedLayout == 'type':
        for typeName in typeList:
            if typeName in data[cardID]['type_line']:
                columnName = typeName
                continue
    elif selectedLayout == 'cost':
        if 'Land' in data[cardID]['type_line']:
            columnName = 'Land'
        else:
            columnName = str(data[cardID]['cmc'])
    window.deckColumns[columnName]['count'] -= 1
    columnCount = window.deckColumns[columnName]['count']
    window.updateColumnCount(window.deckColumns[columnName], columnName, columnCount)
    window.updateDeckHeader()
    if columnCount < 1:
        widget = window.deckColumns[columnName]['widget']
        window.deckLayout.removeWidget(widget)
        widget.deleteLater()
        widget = None
        window.deckColumns.pop(columnName)
    deckData[selectedDeck].remove(cardID)
    deckButtons.remove(cardID, button)
    if len(deckButtons[cardID]) == 0:
        deckButtons.pop(cardID)
    if name in listButtons:
        listButtons.enableCheck(name)
        listButtons.setToolTip(name)

def loadListTooltips():
    for cardName in listButtons:
        listButtons.setToolTip(cardName)

def loadList(listData):
    listButtons.clear()
    for cardID in listData:
        listButtons.add(cardID)
    listButtons.enableCheckAll()
    loadListTooltips()

def getData():
    global data
    data = database.getData()

def getDeckData():
    global deckData
    deckData = database.getDeckData()

def loadSetSelection():
    global setDict
    setDict = database.loadSetDict(data)
    window.setSelectionList.clear()
    window.setSelectionList.addItem('Card set filter')
    setDict = dict(sorted(setDict.items(), key=lambda item: item[1]['date'], reverse=True))
    for index, cardSet in enumerate(setDict):
        setName = setDict[cardSet]['name']
        setCode = setDict[cardSet]['symbol']
        window.setSelectionList.addItem(f"{setName} ({setCode})")
        window.setSelectionList.setItemData(index + 1, setCode)

def loadTypeSelection():
    global typeList
    window.typeSelectionList.clear()
    window.typeSelectionList.addItem('Card type filter')
    orderedList = list(typeList)
    orderedList.sort()
    for typeName in typeList:
        window.typeSelectionList.addItem(typeName)

def loadLayoutSelection():
    window.layoutSelectionList.clear()
    window.layoutSelectionList.addItem('Layout: Card type')
    window.layoutSelectionList.setItemData(0, 'type')
    window.layoutSelectionList.addItem('Layout: Mana cost')
    window.layoutSelectionList.setItemData(1, 'cost')

def loadDeckSelection():
    window.deckSelectionList.clear()
    for deckName in deckData:
        window.deckSelectionList.addItem(deckName)

def clearDeckHeaders():
    window.deckColumns.clear()
    for index in reversed(range(window.deckLayout.count())):
        window.deckLayout.itemAt(index).widget().setParent(None)

def loadDeck(deckName):
    global selectedDeck
    selectedDeck = deckName
    deckButtons.clear()
    clearDeckHeaders()
    window.updateDeckHeader()
    getDeckData()
    dataFilter()
    if deckName == '':
        pass
    else:
        deckList = deckData[deckName]
        if deckList == []:
            pass
        else:
            for cardID in deckList:
                deckButtons.add(cardID)

def loadDeckLayout():
    buttonData = {}
    for cardID in deckButtons:
        buttonData[cardID] = len(deckButtons[cardID])
    deckButtons.clear()
    clearDeckHeaders()
    for cardID in buttonData:
        for index in range(buttonData[cardID]):
            deckButtons.add(cardID)

def saveDeck():
    global selectedDeck
    deckName = selectedDeck
    reload = False
    if deckName == '':
        reload = True
        deckName = window.newDeckEntry.text()
        window.newDeckEntry.setText('')
        if deckName == '':
            return 'No deck name entered.'
    cardList = deckButtons.getString()
    database.saveDeck(deckName, cardList)
    database.commit()
    if reload:
        getDeckData()
        loadDeckSelection()
        window.deckSelectionList.setCurrentText(deckName)
        selectedDeck = deckName
    return f"{deckName} - deck created."

def deleteDeck():
    global selectedDeck
    if selectedDeck != '':
        if window.confirmDeckDelete(selectedDeck) == '&No':
            return
    database.deleteDeck(selectedDeck)
    database.commit()
    getDeckData()
    loadDeckSelection()
    window.deckSelectionList.setCurrentIndex(0)
    selectedDeck = window.deckSelectionList.currentText()
    loadDeck(selectedDeck)

def createDeck(deckName):
    newDeck, infoText = database.createDeck(deckName)
    if newDeck:
        loadDeck(deckName)
        loadDeckSelection()
        window.deckSelectionList.setCurrentText(deckName)
        window.newDeckEntry.setText('')

def colorFilters():
    includeList = []
    excludeList = []
    colorList = []
    colorList.append({'color': 'W', 'state': window.filterWhite.getState()})
    colorList.append({'color': 'U', 'state': window.filterBlue.getState()})
    colorList.append({'color': 'B', 'state': window.filterBlack.getState()})
    colorList.append({'color': 'R', 'state': window.filterRed.getState()})
    colorList.append({'color': 'G', 'state': window.filterGreen.getState()})
    colorList.append({'color': '[]', 'state': window.filterColorless.getState()})
    for entry in colorList:
        if entry['state']:
            includeList.append(entry['color'])
        elif entry['state'] is False:
            excludeList.append(entry['color'])
    return includeList, excludeList

def colorFilterReset():
    window.filterWhite.setState(None)
    window.filterBlue.setState(None)
    window.filterBlack.setState(None)
    window.filterRed.setState(None)
    window.filterGreen.setState(None)
    window.filterColorless.setState(None)
    return [], []

def totalFilterReset():
    window.setSelectionList.setCurrentIndex(0)
    window.typeSelectionList.setCurrentIndex(0)
    window.textSearchBox.setText('')
    window.typeSearchBox.setText('')
    window.nameSearchBox.setText('')
    colorFilterReset()
    loadList(data)

def getFilterStates():
    stateList = []
    stateList.append(None if window.setSelectionList.currentIndex() == 0 else window.setSelectionList.currentText())
    stateList.append(None if window.typeSelectionList.currentIndex() == 0 else window.typeSelectionList.currentText())
    stateList.append(None if window.textSearchBox.text() == '' else window.nameSearchBox.text())
    stateList.append(None if window.typeSearchBox.text() == '' else window.typeSearchBox.text())
    stateList.append(None if window.nameSearchBox.text() == '' else window.nameSearchBox.text())
    stateList.append(window.filterWhite.getState())
    stateList.append(window.filterBlue.getState())
    stateList.append(window.filterBlack.getState())
    stateList.append(window.filterRed.getState())
    stateList.append(window.filterGreen.getState())
    stateList.append(window.filterColorless.getState())
    return stateList

def filterIn(checkList, check):
    return any(item in check for item in checkList)

def filterOut(checkList, check):
    return any(item not in check for item in checkList)

def dataFilter(*signal):
    filteredData = dict(data)
    if 'MiddleButton' in signal:
        colorInclude, colorExclude = colorFilterReset()
    else:
        colorInclude, colorExclude = colorFilters()
    for cardID in data:
        if window.setSelectionList.currentIndex() != 0:
            setName = window.setSelectionList.currentData()
            if filteredData[cardID]['card_set'] != setName:
                filteredData.pop(cardID)
                continue
        if window.typeSelectionList.currentIndex() != 0:
            typeName = window.typeSelectionList.currentText()
            if typeName not in filteredData[cardID]['type_line']:
                filteredData.pop(cardID)
                continue
        if window.textSearchBox.text().lower() not in data[cardID]['oracle_text'].lower():
            filteredData.pop(cardID)
            continue
        if window.typeSearchBox.text().lower() not in data[cardID]['type_line'].lower():
            filteredData.pop(cardID)
            continue
        if window.nameSearchBox.text().lower() not in data[cardID]['name'].lower():
            filteredData.pop(cardID)
            continue
        if filterOut(colorInclude, data[cardID]['color_identity']):
            filteredData.pop(cardID)
            continue
        if filterIn(colorExclude, data[cardID]['color_identity']):
            filteredData.pop(cardID)
            continue
    loadList(filteredData)
    window.setFocus()

def keyCombos(signal):
    if signal == 82:
        if any(state is not None for state in getFilterStates()):
            totalFilterReset()
    elif signal == 83:
        saveDeck()

listButtons = ListButton()
deckButtons = DeckButton()
data = {}
deckData = {}
deckList = []
setDict = {}
selectedDeck = str()
typeList = typeList = ['Land', 'Creature', 'Instant', 'Sorcery', 'Enchantment', 'Artifact', 'Planeswalker']

database = DatabaseHandler.dataHandler()
application = GUI.Application()
window = GUI.Window()
window.setWindowTitle('ArchiveStack')
window.windowStyle('Stylesheet.css')
window.windowIcon('Icon.png')
window.typeList = typeList
ioWindow = GUI.IOWindow()

def threadKill(worker, thread):
    if thread.isRunning():
        worker.stop()
        thread.quit()
        # thread.wait()

def updateStatus(value):
    ioWindow.outputField.append(value)

def setProgressMax(value):
    ioWindow.progressBar.setMaximum(value)

def updateProgress(value):
    ioWindow.progressBar.setValue(value)

def toggleMainWindow(value):
    window.setEnabled(value)

def ioWindowClear():
    ioWindow.inputField.clear()
    ioWindow.outputField.clear()
    ioWindow.progressBar.setValue(0)
    ioWindow.confirmButton.setEnabled(True)

class Worker(GUI.QObject):
    progressMaxSignal = GUI.Signal(int)
    progressSignal = GUI.Signal(int)
    outputSignal = GUI.Signal(str)
    toggleSignal = GUI.Signal(bool)
    def __init__(self):
        super().__init__()
        self.running = False

    def stop(self):
        self.running = False
        ioWindow.confirmButton.setEnabled(True)
        ioWindow.cancelButton.setEnabled(False)

    def clearThread(self):
        ioWindow.confirmButton.setEnabled(True)
        ioWindow.cancelButton.setEnabled(False)
        threadKill(self, ioThread)

class ImportWorker(Worker):
    def __init__(self):
        super().__init__()

    def run(self):
        self.running = True
        inputData = ioWindow.inputField.toPlainText().split('\n')
        if inputData == ['']:
                self.clearThread()
                return
        self.toggleSignal.emit(True)
        ioWindow.cancelButton.setEnabled(True)
        connection, cursor = database.newConnection()
        ###Import from file.###
        # inputData = database.readFile(inputPath)
        translatedData = database.translateData(inputData)
        self.progressMaxSignal.emit(len(translatedData))
        self.outputSignal.emit('Card import started.\n')
        for index, card in enumerate(translatedData):
            if not self.running:
                self.outputSignal.emit(f"\n Card import canceled.")
                self.clearThread()
                return
            self.outputSignal.emit(database.importCard(card, cursor))
            database.commit(connection)
            self.progressSignal.emit(index + 1)
        self.outputSignal.emit('\nCard import complete.\n')
        connection.close()
        self.clearThread()

class ExportWorker(Worker):
    def __init__(self):
        super().__init__()

    def run(self):
        output = ''
        for cardID in deckButtons:
            output += f"{len(deckButtons[cardID])} {data[cardID]['name']}({data[cardID]['card_set']})\n"
        output = output.rstrip('\n')
        ###Output to file.###
        # outputFile = open(outputPath, 'w')
        # outputFile.write(output)
        # outputFile.close()
        self.outputSignal.emit(output)
        self.clearThread()

class RemovalWorker(Worker):
    def __init__(self):
        super().__init__()

    def run(self):
        self.running = True
        inputData = ioWindow.inputField.toPlainText().split('\n')
        if inputData == ['']:
            self.clearThread()
            return
        self.toggleSignal.emit(True)
        connection, cursor = database.newConnection()
        minimumCounts = {}
        for deckName in deckData:
            for cardID in deckData[deckName]:
                if cardID in minimumCounts:
                    minimumCounts[cardID] += 1
                else:
                    minimumCounts[cardID] = 1
        ###Removal from file.###
        # inputData = database.readFile(removePath)
        translatedData = database.translateData(inputData)
        self.outputSignal.emit('Card removal started.\n')
        for card in translatedData:
            output = database.removeCard(card, minimumCounts, cursor)
            for line in output:
                self.outputSignal.emit(line)
        self.outputSignal.emit('\nCard removal complete.\n')
        database.commit(connection)
        connection.close()
        self.clearThread()

class SymbologyWorker(Worker):
    def __init__(self):
        super().__init__()

    def run(self):
        self.running = True
        connection, cursor = database.newConnection()
        self.toggleSignal.emit(True)
        ioWindow.cancelButton.setEnabled(True)
        symbolTypes = ['symbology', 'sets']
        self.outputSignal.emit('Symbology update started.')
        for symbolType in symbolTypes:
            if symbolType == 'symbology':
                typeName = 'symbol'
                typeURL = 'svg_uri'
                emitName = 'Card'
            elif symbolType == 'sets':
                typeName = 'code'
                typeURL = 'icon_svg_uri'
                emitName = 'Set'
            else:
                return
            self.outputSignal.emit(f"\n{emitName} symbology update started.\n")
            symbolPath = os.path.join(path, 'images', symbolType)
            if not os.path.isdir(symbolPath):
                os.makedirs(symbolPath)
            symbologyData = database.fetchSymbologyData(symbolType)
            self.progressMaxSignal.emit(len(symbologyData))
            for index, symbol in enumerate(symbologyData):
                if not self.running:
                    self.outputSignal.emit(f"\n Symbology update canceled.")
                    connection.close()
                    self.clearThread()
                    return
                if symbolType == 'sets':
                    self.outputSignal.emit(symbol['name'])
                elif symbolType == 'symbology':
                    self.outputSignal.emit(symbol['symbol'])
                database.fetchSymbol(symbol, symbolType, typeName, typeURL, symbolPath, cursor)
                self.progressSignal.emit(index + 1)
            database.commit(connection)
            self.outputSignal.emit(f"\n{emitName} symbology update complete.")
        self.outputSignal.emit('\nSymbology update complete.\n')
        connection.close()
        self.clearThread()

def ioWindowConnect(worker, workerName, thread):
    thread.worker = worker
    thread.workerName = workerName
    worker.progressMaxSignal.connect(setProgressMax)
    worker.progressSignal.connect(updateProgress)
    worker.outputSignal.connect(updateStatus)
    worker.toggleSignal.connect(toggleButtons)
    thread.started.connect(worker.run)
    ioWindow.cancelButton.clicked.connect(lambda: worker.stop())
    ioWindow.confirmButton.clicked.connect(thread.start)
    ioWindow.closeSignal.connect(lambda: threadKill(worker, thread))

def ioWindowDisconnect(thread):
    thread.worker.progressMaxSignal.disconnect()
    thread.worker.progressSignal.disconnect()
    thread.worker.outputSignal.disconnect() 
    thread.worker.toggleSignal.disconnect()
    thread.started.disconnect()
    ioWindow.cancelButton.clicked.disconnect()
    ioWindow.confirmButton.clicked.disconnect()
    ioWindow.closeSignal.disconnect()
    ioWindow.closeSignal.connect(lambda: ioWindowClosed())
    thread.worker = None 

def ioWindowClosed():
    toggleMainWindow(True)
    ioWindowDisconnect(ioThread)

def ioImport():
    ioWindowClear()
    toggleMainWindow(False)
    ioWindowConnect(importWorker, 'import', ioThread)
    ioWindow.setWindowTitle('Import cards')
    ioWindow.windowStyle('Stylesheet.css')
    ioWindow.windowIcon('Icon.png')
    ioWindow.confirmButton.setText('Import')
    ioWindow.contentContainer.setMinimumSize(420, 670)
    ioWindow.confirmButton.setHidden(False)
    ioWindow.cancelButton.setHidden(False)
    ioWindow.cancelButton.setEnabled(False)
    ioWindow.clearButton.setHidden(False)
    ioWindow.outputField.setHidden(True)
    ioWindow.progressBar.setHidden(True)
    ioWindow.inputField.setHidden(False)
    ioWindow.inputField.setPlaceholderText("""Import format:
Number(card count), space, card name
Writing 'set=setcode' on a line will try to import all cards after that line from that set.
Writing the setcode in brackets after the card name will try to import that card from that set.
special setcodes: NONE(Resets any prevously set setcode)

Examples:
2 Island        - this line will import 2 Island cards from a recent set
set=LEA       - this line will make the following lines import cards from Limited Edition Alpha
1 Island(leb) - this line will import 1 Island card from Limited Edition Beta 
1 Island        - this line will import 1 Island card from Limited Edition Alpha
""")
    ioWindow.show()

def ioExport():
    ioWindowClear()
    ioWindowConnect(exportWorker, 'export', ioThread)
    toggleMainWindow(False)
    ioWindow.setWindowTitle('Export cards')
    ioWindow.windowStyle('Stylesheet.css')
    ioWindow.windowIcon('Icon.png')
    ioWindow.contentContainer.setMinimumSize(420, 670)
    ioWindow.outputField.setHidden(False)
    ioWindow.inputField.setHidden(True)
    ioWindow.progressBar.setHidden(True)
    ioWindow.confirmButton.setHidden(True)
    ioWindow.cancelButton.setHidden(True)
    ioWindow.clearButton.setHidden(True)
    ioWindow.show()
    exportWorker.run()

def ioRemoval():
    ioWindowClear()
    ioWindowConnect(removalWorker, 'removal', ioThread)
    toggleMainWindow(False)
    ioWindow.setWindowTitle('Remove cards')
    ioWindow.windowStyle('Stylesheet.css')
    ioWindow.windowIcon('Icon.png')
    ioWindow.confirmButton.setText('Remove')
    ioWindow.contentContainer.setMinimumSize(420, 670)
    ioWindow.confirmButton.setHidden(False)
    ioWindow.cancelButton.setHidden(False)
    ioWindow.cancelButton.setEnabled(False)
    ioWindow.clearButton.setHidden(False)
    ioWindow.outputField.setHidden(True)
    ioWindow.progressBar.setHidden(True)
    ioWindow.inputField.setHidden(False)
    ioWindow.inputField.setPlaceholderText("""Import format:
Number(card count), space, card name
Writing 'set=setcode' on a line will remove all cards after that line from that set.
Writing the setcode in brackets after the card name will try to remove that card from that set.
special setcodes: ANY(Removes cards from any set), ALL(Removes cards from all sets)

Examples:
2 Island        - this line will remove 2 Island cards from a recent set
set=LEA       - this line will make the following lines remove cards from Limited Edition Alpha
1 Island(leb) - this line will remove 1 Island card from Limited Edition Beta 
1 Island        - this line will remove 1 Island card from Limited Edition Alpha
""")
    ioWindow.show()

def ioSymbology():
    ioWindowClear()
    ioWindowConnect(symbologyWorker, 'symbology', ioThread)
    toggleMainWindow(False)
    ioWindow.setWindowTitle('Update symbols')
    ioWindow.windowStyle('Stylesheet.css')
    ioWindow.windowIcon('Icon.png')
    ioWindow.confirmButton.setText('Start update')
    ioWindow.contentContainer.setMinimumSize(420, 270)
    ioWindow.inputField.setHidden(True)
    ioWindow.confirmButton.setHidden(False)
    ioWindow.clearButton.setHidden(True)
    ioWindow.cancelButton.setHidden(False)
    ioWindow.cancelButton.setEnabled(False)
    ioWindow.outputField.setHidden(False)
    ioWindow.show()

def toggleButtons():
    ioWindow.progressBar.setHidden(False)
    ioWindow.outputField.setHidden(False)
    ioWindow.confirmButton.setEnabled(False)

def finishedWork():
    worker = ioThread.workerName
    if worker == 'import':
        getData()
        loadList(data)
        loadSetSelection()
    elif worker == 'removal':
        getData()
        getDeckData()
        loadDeck(selectedDeck)
        loadList(data)
        loadSetSelection()

getData()
getDeckData()
if len(deckData):
    selectedDeck = list(deckData)[0]
loadDeckSelection()
loadSetSelection()
loadTypeSelection()
loadLayoutSelection()
loadDeck(selectedDeck)

ioThread = GUI.Thread()
importWorker = ImportWorker()
importWorker.moveToThread(ioThread)
exportWorker = ExportWorker()
exportWorker.moveToThread(ioThread)
removalWorker = RemovalWorker()
removalWorker.moveToThread(ioThread)
symbologyWorker = SymbologyWorker()
symbologyWorker.moveToThread(ioThread)
ioThread.finished.connect(finishedWork)

ioWindow.closeSignal.connect(lambda: ioWindowClosed())
ioWindow.clearButton.clicked.connect(lambda: ioWindow.inputField.clear())

window.closeSignal.connect(application.quit)

window.importButton.clicked.connect(lambda: ioImport())
window.exportButton.clicked.connect(lambda: ioExport())
window.removalButton.clicked.connect(lambda: ioRemoval())
window.symbologyButton.clicked.connect(lambda: ioSymbology())

window.newDeckEntry.returnPressed.connect(lambda: createDeck(window.newDeckEntry.text()))
window.deckSelectionList.textActivated.connect(loadDeck)
window.saveButton.clicked.connect(lambda: saveDeck())
window.deleteButton.clicked.connect(lambda: deleteDeck())

window.layoutSelectionList.textActivated.connect(loadDeckLayout)
window.setSelectionList.textActivated.connect(dataFilter)
window.typeSelectionList.textActivated.connect(dataFilter)
window.filterWhite.clicked.connect(dataFilter)
window.filterBlue.clicked.connect(dataFilter)
window.filterBlack.clicked.connect(dataFilter)
window.filterRed.clicked.connect(dataFilter)
window.filterGreen.clicked.connect(dataFilter)
window.filterColorless.clicked.connect(dataFilter)
window.typeSearchBox.returnPressed.connect(lambda: dataFilter())
window.nameSearchBox.returnPressed.connect(lambda: dataFilter())
window.textSearchBox.returnPressed.connect(lambda: dataFilter())

window.keyPressed.connect(keyCombos)

window.show()

application.launch()
