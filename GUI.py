#!/usr/bin/python
#coding=utf-8
import sys, os
from PySide6.QtWidgets import QApplication, QComboBox, QHBoxLayout, QLabel, QMessageBox, QProgressBar, QScrollArea, QTextEdit, QVBoxLayout, QWidget, QMainWindow, QLineEdit, QPushButton
from PySide6.QtGui import QFontDatabase, QIcon, QPixmap
from PySide6.QtCore import QObject, QThread, Qt, Signal

class Application(QApplication):
    def __init__(self):
        super().__init__()

    def launch(self):
        sys.exit(self.exec_())

class Thread(QThread):
    def __init__(self):
        super().__init__()
        self.worker = None
        self.workerName = ''

class MainWindow(QMainWindow):
    closeSignal = Signal(bool)
    def __init__(self):
        super().__init__()
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.symbolPath = os.path.join(self.path, 'images', 'symbology')
        self.directory = 'bin'
        self.typefacePath = os.path.join(self.path, self.directory, 'typeface.ttf')
        QFontDatabase.addApplicationFont(self.typefacePath)
        self._create()

    def closeEvent(self, event):
        self.closeSignal.emit(True)
        super().closeEvent(event)

    def _create(self):
        self._createMain()

    def _createMain(self):
        self.mainLayout = QVBoxLayout()
        self.mainLayout.setSpacing(0)
        self.mainLayout.setContentsMargins(0, 0, 0, 0)
        self.mainLayout.setAlignment(Qt.AlignTop)
        self.mainWidget = QWidget()
        self.mainWidget.setLayout(self.mainLayout)
        self.setCentralWidget(self.mainWidget)

    def windowIcon(self, iconPath):
        iconPath = os.path.join(self.path, self.directory, iconPath)
        icon = QIcon()
        icon.addPixmap(QPixmap(iconPath))
        self.setWindowIcon(icon)

    def windowStyle(self, stylePath):
        stylePath = os.path.join(self.path, self.directory, stylePath)
        with open(stylePath) as styleSheet:
            self.setStyleSheet(styleSheet.read())

class IOWindow(MainWindow):
    def __init__(self):
        super().__init__()

    def _create(self):
        super()._create()
        self._createContent()

    def _createContent(self):
        self.contentLayout = QVBoxLayout()
        self.contentLayout.setContentsMargins(10, 10, 10, 10)
        self.contentLayout.setSpacing(5)
        self.contentContainer = QWidget()
        self.contentContainer.setMinimumSize(420, 670)
        self.contentContainer.setLayout(self.contentLayout)

        self.outputLayout = QVBoxLayout()
        self.outputLayout.setSpacing(5)
        self.outputLayout.setContentsMargins(0, 0, 0, 0)
        self.outputContainer = QWidget()
        self.outputContainer.setLayout(self.outputLayout)

        self.inputField = QTextEdit()
        self.inputField.setMinimumSize(500, 400)

        self.buttonLayout = QHBoxLayout()
        self.buttonLayout.setSpacing(5)
        self.buttonLayout.setContentsMargins(0, 0, 0, 0)
        self.buttonLayout.setAlignment(Qt.AlignRight)
        self.buttonContainer = QWidget()
        self.buttonContainer.setLayout(self.buttonLayout)
        self.confirmButton = QPushButton()
        self.confirmButton.setFixedSize(100, 20)
        self.cancelButton = QPushButton('Cancel')
        self.cancelButton.setFixedSize(100, 20)
        self.clearButton = QPushButton('Clear input')
        self.clearButton.setFixedSize(100, 20)

        self.outputField = QTextEdit()
        self.outputField.setReadOnly(True)
        self.progressBar = QProgressBar()
        self.progressBar.setFixedHeight(20)

        self.contentLayout.addWidget(self.inputField)
        self.outputLayout.addWidget(self.progressBar)
        self.outputLayout.addWidget(self.outputField)
        self.contentLayout.addWidget(self.outputContainer)
        self.buttonLayout.addWidget(self.clearButton)
        self.buttonLayout.addWidget(self.cancelButton)
        self.buttonLayout.addWidget(self.confirmButton)
        self.contentLayout.addWidget(self.buttonContainer)
        self.mainLayout.addWidget(self.contentContainer)

    def updateProgress(self, value):
        self.progressBar.setValue(value)

class Window(MainWindow):
    def __init__(self):
        self.deckColumns = {}
        self.typeList = []
        super().__init__()

    def _create(self):
        super()._create()
        self._createControlBar()
        self._createFilterBar()
        self._createContent()
        self._createCardList()
        self._createDeck()

    def _createContent(self):
        self.contentContainer = QWidget()
        self.contentLayout = QHBoxLayout()
        self.contentLayout.setSpacing(0)
        self.contentLayout.setContentsMargins(0, 0, 0, 0)
        self.contentLayout.setAlignment(Qt.AlignTop)
        self.contentContainer.setLayout(self.contentLayout)
        self.mainLayout.addWidget(self.contentContainer)

    def _createCardList(self):
        self.cardListLayout = QVBoxLayout()
        self.cardListLayout.setSpacing(1)
        self.cardListLayout.setContentsMargins(0, 0, 0, 0)
        self.cardListLayout.setAlignment(Qt.AlignTop)
        self.cardListContainer = QWidget()
        self.cardListContainer.setLayout(self.cardListLayout)
        self.cardListContainer.setStyleSheet('background-color: #999;')

        self.cardListScroll = QScrollArea()
        self.cardListScroll.setFixedWidth(250)
        self.cardListScroll.setWidgetResizable(True)
        self.cardListScroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.cardListScroll.setWidget(self.cardListContainer)
        self.contentLayout.addWidget(self.cardListScroll)

    def _createDeck(self):
        self.deckSpacer = QWidget()
        self.deckSpacer.setFixedWidth(5)
        self.deckMain = QWidget()
        self.deckMainLayout = QVBoxLayout()
        self.deckMainLayout.setSpacing(0)
        self.deckMainLayout.setContentsMargins(0, 0, 0, 0)
        self.deckHeader = QLabel()
        self.deckHeader.setFixedHeight(20)
        self.deckHeader.setAlignment(Qt.AlignCenter)
        self.deckHeader.setStyleSheet('background-color: #999;')
        self.deckMain.setLayout(self.deckMainLayout)
        self.deckContainer = QWidget()
        self.deckLayout = QHBoxLayout()
        self.deckLayout.setSpacing(1)
        self.deckLayout.setContentsMargins(0, 0, 0, 0)
        self.deckContainer.setLayout(self.deckLayout)
        self.deckContainer.setMinimumSize(800, 500)
        self.deckContainer.setStyleSheet('background-color: #999;')
        self.deckMainLayout.addWidget(self.deckHeader)
        self.deckMainLayout.addWidget(self.deckContainer)
        self.contentLayout.addWidget(self.deckSpacer)
        self.contentLayout.addWidget(self.deckMain)

    def _createControlBar(self):
        self.controlBar = QWidget()
        self.controlLayout = QHBoxLayout()
        self.controlBar.setLayout(self.controlLayout)

        self.IOButtonContainer = QWidget()
        self.IOButtonLayout = QHBoxLayout()
        self.IOButtonContainer.setLayout(self.IOButtonLayout)
        self.IOButtonLayout.setSpacing(1)
        self.IOButtonLayout.setContentsMargins(0, 0, 0, 0)
        self.IOButtonLayout.setAlignment(Qt.AlignTop)
        self.importButton = QPushButton('Import cards')
        self.importButton.setFixedSize(100, 20)
        self.exportButton = QPushButton('Export cards')
        self.exportButton.setFixedSize(100, 20)
        self.removalButton = QPushButton('Remove cards')
        self.removalButton.setFixedSize(100, 20)
        self.symbologyButton = QPushButton('Update symbols')
        self.symbologyButton.setFixedSize(100, 20)
        self.IOButtonLayout.addWidget(self.importButton)
        self.IOButtonLayout.addWidget(self.exportButton)
        self.IOButtonLayout.addWidget(self.removalButton)
        self.IOButtonLayout.addWidget(self.symbologyButton)

        self.layoutSelectionList = QComboBox()
        self.layoutSelectionList.setFixedSize(150, 20)
        self.deckButtonContainer = QWidget()
        self.deckButtonLayout = QHBoxLayout()
        self.deckButtonContainer.setLayout(self.deckButtonLayout)
        self.deckButtonLayout.setSpacing(1)
        self.deckButtonLayout.setContentsMargins(0, 0, 0, 0)
        self.deckButtonLayout.setAlignment(Qt.AlignRight)
        self.saveButton = QPushButton('Save deck')
        self.saveButton.setFixedSize(80, 20)
        self.newDeckEntry = QLineEdit()
        self.newDeckEntry.setPlaceholderText('Enter new deck name')
        self.newDeckEntry.setFixedSize(150, 20)
        self.deleteButton = QPushButton('Delete deck')
        self.deleteButton.setFixedSize(80, 20)
        self.deckSelectionList = QComboBox()
        self.deckSelectionList.setFixedSize(150, 20)
        self.deckButtonLayout.addWidget(self.layoutSelectionList)
        self.deckButtonLayout.addWidget(self.newDeckEntry)
        self.deckButtonLayout.addWidget(self.deckSelectionList)
        self.deckButtonLayout.addWidget(self.saveButton)
        self.deckButtonLayout.addWidget(self.deleteButton)

        self.controlLayout.addWidget(self.IOButtonContainer)
        self.controlLayout.addWidget(self.deckButtonContainer)
        self.mainLayout.addWidget(self.controlBar)

    def _createFilterBar(self):
        self.filterBar = QWidget()
        self.filterLayout = QHBoxLayout()
        self.filterBar.setLayout(self.filterLayout)
        self.filterLayout.setAlignment(Qt.AlignLeft)
        self.setSelectionList = QComboBox()
        self.setSelectionList.setFixedSize(150, 20)
        self.typeSelectionList = QComboBox()
        self.typeSelectionList.setFixedSize(150, 20)
        self.textSearchBox = QLineEdit()
        self.textSearchBox.setPlaceholderText('Card text search')
        self.textSearchBox.setFixedSize(150, 20)
        self.typeSearchBox = QLineEdit()
        self.typeSearchBox.setPlaceholderText('Card type search')
        self.typeSearchBox.setFixedSize(150, 20)
        self.nameSearchBox = QLineEdit()
        self.nameSearchBox.setPlaceholderText('Card name search')
        self.nameSearchBox.setFixedSize(150, 20)
        # self.setSelectionList.view().setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        # self.setSelectionList.setStyleSheet("combobox-popup: 0;")

        self.colorFilterContainer = QWidget()
        self.colorFilterContainer.setFixedSize(250, 30)
        self.colorFilterLayout = QHBoxLayout()
        self.colorFilterContainer.setLayout(self.colorFilterLayout)
        colorToggle = {None: '', True: 'solid #0f0', False: 'solid #f00'}

        iconWhite = QIcon()
        iconWhite.addPixmap(QPixmap(os.path.join(self.symbolPath, 'symbology_W.svg')))
        self.filterWhite = ToggleButton(colorToggle)
        self.filterWhite.setIcon(iconWhite)
        self.filterWhite.setFixedSize(20, 20)

        iconBlue = QIcon()
        iconBlue.addPixmap(QPixmap(os.path.join(self.symbolPath, 'symbology_U.svg')))
        self.filterBlue = ToggleButton(colorToggle)
        self.filterBlue.setIcon(iconBlue)
        self.filterBlue.setFixedSize(20, 20)

        iconBlack = QIcon()
        iconBlack.addPixmap(QPixmap(os.path.join(self.symbolPath, 'symbology_B.svg')))
        self.filterBlack = ToggleButton(colorToggle)
        self.filterBlack.setIcon(iconBlack)
        self.filterBlack.setFixedSize(20, 20)

        iconRed = QIcon()
        iconRed.addPixmap(QPixmap(os.path.join(self.symbolPath, 'symbology_R.svg')))
        self.filterRed = ToggleButton(colorToggle)
        self.filterRed.setIcon(iconRed)
        self.filterRed.setFixedSize(20, 20)

        iconGreen = QIcon()
        iconGreen.addPixmap(QPixmap(os.path.join(self.symbolPath, 'symbology_G.svg')))
        self.filterGreen = ToggleButton(colorToggle)
        self.filterGreen.setIcon(iconGreen)
        self.filterGreen.setFixedSize(20, 20)

        iconColorless = QIcon()
        iconColorless.addPixmap(QPixmap(os.path.join(self.symbolPath, 'symbology_C.svg')))
        self.filterColorless = ToggleButton(colorToggle)
        self.filterColorless.setIcon(iconColorless)
        self.filterColorless.setFixedSize(20, 20)

        self.colorFilterLayout.addWidget(self.filterWhite)
        self.colorFilterLayout.addWidget(self.filterBlue)
        self.colorFilterLayout.addWidget(self.filterBlack)
        self.colorFilterLayout.addWidget(self.filterRed)
        self.colorFilterLayout.addWidget(self.filterGreen)
        self.colorFilterLayout.addWidget(self.filterColorless)

        self.filterLayout.addWidget(self.setSelectionList)
        self.filterLayout.addWidget(self.typeSelectionList)
        self.filterLayout.addWidget(self.colorFilterContainer)
        self.filterLayout.addWidget(self.textSearchBox)
        self.filterLayout.addWidget(self.typeSearchBox)
        self.filterLayout.addWidget(self.nameSearchBox)

        self.mainLayout.addWidget(self.filterBar)

    def updateDeckHeader(self):
        headerText = 0
        for column in self.deckColumns:
            headerText += int(self.deckColumns[column]['count'])
        self.deckHeader.setText(str(headerText))

    def updateColumnCount(self, column, name, count):
        column['label'].setText(f"{name} ({count})")

    def createDeckColumn(self, title):
        columnWidget = QWidget()
        columnLayout = QVBoxLayout()
        columnLayout.setSpacing(1)
        columnLayout.setContentsMargins(0, 0, 0, 0)
        columnLayout.setAlignment(Qt.AlignTop)
        columnWidget.setLayout(columnLayout)
        columnLabel = QLabel(title)
        columnLabel.setAlignment(Qt.AlignCenter)
        columnLayout.addWidget(columnLabel)
        return columnWidget, columnLayout, columnLabel

    def addButton(self, layout, data):
        name = data['name']
        button = CardButton()
        button.setText(name)
        if layout == 'deck':
            layoutType = self.layoutSelectionList.currentData()
            if layoutType == 'type':
                cardType = ''
                for typeName in self.typeList:
                    if typeName in data['type_line']:
                        if typeName not in self.deckColumns:
                            cardType = typeName
                            columnWidget, columnLayout, columnLabel = self.createDeckColumn(cardType)
                            columnLabel.setText(f"{columnLabel.text()} (1)")
                            self.deckColumns[typeName] = {'layout': columnLayout, 'widget': columnWidget, 'label': columnLabel, 'name': typeName, 'count': 1}
                            self.deckColumns = dict(sorted(self.deckColumns.items(), key=lambda item: self.typeList.index(item[0])))
                            index = list(self.deckColumns.keys()).index(typeName)
                            self.deckLayout.insertWidget(index, columnWidget)
                            columnLayout.addWidget(button)
                        else:
                            column = self.deckColumns[typeName]
                            columnText = column['label'].text().split(' ')[0]
                            columnCount = column['layout'].count()
                            column['count'] = columnCount
                            self.updateColumnCount(column, columnText, columnCount)
                            column['layout'].addWidget(button)
                        self.updateDeckHeader()
                        return button
            elif layoutType == 'cost':
                if 'land' in data['type_line'].lower():
                    cardCost = 'Land'
                else:
                    cardCost = str(data['cmc'])
                if cardCost not in self.deckColumns:
                    columnWidget, columnLayout, columnLabel = self.createDeckColumn(cardCost)
                    columnLabel.setText(f"{columnLabel.text()} (1)")
                    self.deckColumns[cardCost] = {'layout': columnLayout, 'widget': columnWidget, 'label': columnLabel, 'name': cardCost, 'count': 1}
                    self.deckColumns = sorted(self.deckColumns.items())
                    if 'Land' in self.deckColumns[len(self.deckColumns) - 1]:
                        self.deckColumns.insert(0, self.deckColumns.pop(len(self.deckColumns) - 1))
                    self.deckColumns = dict(self.deckColumns)
                    index = list(self.deckColumns.keys()).index(cardCost)
                    self.deckLayout.insertWidget(index, columnWidget)
                    columnLayout.addWidget(button)
                else:
                    column = self.deckColumns[cardCost]
                    columnText = column['label'].text().split(' ')[0]
                    columnCount = column['layout'].count()
                    column['count'] = columnCount
                    self.updateColumnCount(column, columnText, columnCount)
                    column['layout'].addWidget(button)
                return button
        elif layout == 'list':
            self.cardListLayout.addWidget(button)
        return button

    def removeButton(self, layout, button):
        layouts = {'list': self.cardListLayout,
                    'deck': self.deckLayout}
        layouts[layout].removeWidget(button)
        button.deleteLater()
        button = None

    def confirmDeckDelete(self, deckName):
        confirmWindow = QMessageBox()
        confirmWindow.setWindowTitle(deckName)
        confirmWindow.setText(f"Delete {deckName}?")
        confirmWindow.setIcon(QMessageBox.Warning)
        confirmWindow.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        confirmWindow.exec()
        return confirmWindow.clickedButton().text()

    def align(self, alignment):
        alignments = Qt.AlignmentFlag.__dict__
        keys = alignments.keys()
        for key in keys:
            if alignment == key:
                return alignments[key]
        return 'Alignment not found.'

    keyPressed = Signal(int)

    def keyPressEvent(self, event):
        if not event.isAutoRepeat():
            modifier = QApplication.keyboardModifiers()
            if modifier == Qt.ControlModifier:
                self.keyPressed.emit(event.key())

class CardButton(QPushButton):
    def __init__(self):
        super().__init__()

class ToggleButton(QPushButton):
    def __init__(self, stateDict):
        super().__init__()
        self.stateDict = stateDict
        self.state = None
        self.setAppearance()

    def getState(self):
        return self.state

    def setState(self, value):
        self.state = value
        self.setAppearance()

    def setAppearance(self):
        self.appearance = f"border: 4px {self.stateDict[self.state]}; border-radius: 10px"
        self.setStyleSheet(self.appearance)

    def _toggle(self, states):
        index = states.index(self.state)
        index = 0 if index >= len(states) - 1 else index + 1
        return states[index]

    clicked = Signal(str)

    def mousePressEvent(self, signal):
        if signal.button() == Qt.LeftButton:
            self.setState(self._toggle(list(self.stateDict.keys())))
            
        elif signal.button() == Qt.RightButton:
            self.setState(self._toggle(list(self.stateDict.keys())[::-1]))
        self.clicked.emit(str(signal.button()).split('.')[-1])
        # modifier = ''
        # button = ''
        # if signal.modifiers() == Qt.ShiftModifier:
        #     modifier = 'shift - '
        # if signal.buttons() == Qt.LeftButton:
        #     button = 'left'
        # elif signal.buttons() == Qt.RightButton:
        #     button = 'right'
        # elif signal.buttons() == Qt.MiddleButton:
        #     button = 'middle'
        # print(modifier + button)
    