#!/usr/bin/python
#coding=utf-8
import os, re, json, urllib.request, sqlite3
from time import sleep

class dataHandler():
    def __init__(self):
        self.searchURL = 'https://api.scryfall.com/cards/named?exact='
        #https://api.scryfall.com/cards/xln/96
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.directory = 'bin'
        self.imageDirectory = 'images'
        self.dataPath = os.path.join(self.path, self.directory, 'Data.db')
        if not os.path.isfile(self.dataPath):
            # os.mknod(self.dataPath)
            with open(self.dataPath, 'w') as file:
                pass
        self.connection = sqlite3.connect(self.dataPath)
        self.connection.row_factory = sqlite3.Row
        self.cursor = self.connection.cursor()

        create = """CREATE TABLE IF NOT EXISTS cards (
                        'card_id' text,
                        'name' text,
                        'count' integer,
                        'layout' text,
                        'color_identity' text,
                        'mana_cost' text,
                        'cmc' integer,
                        'power' integer,
                        'toughness' integer,
                        'type_line' text,
                        'oracle_text' text,
                        'name_2' text,
                        'mana_cost_2' text,
                        'power_2' integer,
                        'toughness_2' integer,
                        'type_line_2' text,
                        'oracle_text_2' text,
                        'partner' text,
                        'card_set' text
                    )"""
        self.cursor.execute(create)

        create = """CREATE TABLE IF NOT EXISTS decks (
                        'deck_name' text,
                        'card_list' text
                    )"""
        self.cursor.execute(create)

        create = """CREATE TABLE IF NOT EXISTS symbology (
                        'symbol' text,
                        'path' text
                    )"""
        self.cursor.execute(create)

        create = """CREATE TABLE IF NOT EXISTS sets (
                        'name' text,
                        'date' text,
                        'symbol' text,
                        'path' text
                    )"""
        self.cursor.execute(create)

        self.insertStructure = """INSERT INTO cards
                        (card_id, name, count, layout, color_identity, mana_cost, cmc, power, toughness, type_line, oracle_text,
                        name_2, mana_cost_2, power_2, toughness_2, type_line_2, oracle_text_2, partner, card_set)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                        """

        self.doubleFacedCards = ['flip', 'split', 'transform', 'modal_dfc']
        self.colorDictionary = dict({'W': '#e8e6c6',
                                'B': '#aaa19b',
                                'R': '#e49977',
                                'G': '#a3c095',
                                'U': '#c1d7e9',
                                'multi': '#c3ad58',
                                'none': '#cac5c0'})
        self.colorCleanupList = ['[', '\'', ']']

        self.commit()

    def commit(self, *connection):
        if len(connection) != 0:
            connection = connection[0]
        else:
            connection = self.connection
        connection.commit()

    def newConnection(self):
        connection = sqlite3.connect(self.dataPath)
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        return connection, cursor

    def getData(self):
        data = f"SELECT * FROM cards"
        data = self.cursor.execute(data)
        data = {row['card_id']: dict(row) for row in data.fetchall()}
        data = dict(sorted(data.items(), key=lambda item: item[1]['name']))
        return data

    def getDeckData(self):
        deckList = {}
        data = f"SELECT * FROM decks"
        data = self.cursor.execute(data)
        data = data.fetchall()
        for deck in data:
            deckList[deck['deck_name']] = deck['card_list'].split('|')
            if '' in deckList[deck['deck_name']]:
                deckList[deck['deck_name']].remove('')
        return deckList

    def readFile(self, inputPath):
        with open(inputPath, 'r') as file:
            data = file.readlines()
        return data

    def _readLine(self, line, currentSet):
        values = {}
        if line == '':
            return values, currentSet
        values['card_set'] = currentSet
        if 'set=' in line:
            values['card_set'] = re.compile('^.{4}(.+)').match(line).groups()[0]
            currentSet = values['card_set']
            if values['card_set'].upper() == 'NONE':
                values['card_set'] = ''
                currentSet = ''
            return values, currentSet
        if line == '\n' or re.compile('^\d+ .+').match(line) is None:
            return values
        lineValues = re.compile('^(\d+).(.+)').match(line).groups()
        values['card_name'] = lineValues[1]
        values['card_count'] = lineValues[0]
        if '(' in values['card_name'] and ')' in values['card_name']:
            nameLine = values['card_name'].split('(')
            values['card_name'] = nameLine[0]
            values['card_set'] = nameLine[1].replace(')', '').upper()
        return values, currentSet

    def loadSetDict(self, data):
        setDict = {}
        for cardID in data:
            cardSet = data[cardID]['card_set']
            if cardSet not in setDict:
                setData = {}
                dataCheck = f"SELECT * FROM sets WHERE symbol = '{cardSet}'"
                dataCheck = self.cursor.execute(dataCheck)
                dataCheck = dataCheck.fetchall()
                if dataCheck != []:
                    setData['name'] = dataCheck[0]['name']
                    setData['date'] = dataCheck[0]['date']
                    setData['symbol'] = dataCheck[0]['symbol']
                    setDict[cardSet] = setData
        return setDict

    def fetchCardData(self, cardName, cardSet, cardCount):
        try:
            searchResult = urllib.request.urlopen(self.searchURL + re.sub(' ', '+', cardName) + f'&set={cardSet}')
        except urllib.error.HTTPError:
            try:
                searchResult = urllib.request.urlopen(self.searchURL + re.sub(' ', '+', cardName))
            except:
                return None
        cardData = json.loads(searchResult.read().decode())
        cardID = cardData['id']
        layout = cardData['layout']
        cardCMC = cardData['cmc']
        cardColor = cardData['color_identity']
        if layout in self.doubleFacedCards:
            face1 = cardData['card_faces'][0]
            face2 = cardData['card_faces'][1]
            cardName = face1['name']
            cardManaCost = face1['mana_cost']
            if cardManaCost == '':
                cardManaCost = None
            cardTypeLine = face1['type_line']
            cardOracleText = face1['oracle_text']
            if 'power' in face1:
                cardPower = face1['power']
            else:
                cardPower = None
            if 'toughness' in face1:
                cardToughness = face1['toughness']
            else:
                cardToughness = None
            cardName2 = face2['name']
            cardManaCost2 = face2['mana_cost']
            if cardManaCost2 == '':
                cardManaCost2 = None
            cardTypeLine2 = face2['type_line']
            cardOracleText2 = face2['oracle_text']
            if 'power' in face2:
                cardPower2 = face2['power']
            else:
                cardPower2 = None
            if 'toughness' in face2:
                cardToughness2 = face2['toughness']
            else:
                cardToughness2 = None
            cardPartner = None
        else:
            cardName = cardData['name']
            cardManaCost = cardData['mana_cost']
            if cardManaCost == '':
                cardManaCost = None
            cardTypeLine = cardData['type_line']
            cardOracleText = cardData['oracle_text']
            if 'power' in cardData:
                cardPower = cardData['power']
            else:
                cardPower = None
            if 'toughness' in cardData:
                cardToughness = cardData['toughness']
            else:
                cardToughness = None
            cardManaCost2 = None
            cardTypeLine2 = None
            cardOracleText2 = None
            cardPower2 = None
            cardToughness2 = None
            if 'all_parts' in cardData:
                parts = cardData['all_parts']
                if layout == 'meld':
                    cardName2 = parts[2]['name']
                    cardPartner = parts[1]['name']
                else:
                    cardName2 = None
                    cardPartner = parts[1]['name']
            else:
                cardName2 = None
                cardPartner = None
        cardSet = cardData['set'].upper()
        fetchedData = [str(cardID), str(cardName), int(cardCount), str(layout), str(cardColor), str(cardManaCost), int(cardCMC), str(cardPower), str(cardToughness), str(cardTypeLine), str(cardOracleText),
        str(cardName2), str(cardManaCost2), str(cardPower2), str(cardToughness2), str(cardTypeLine2), str(cardOracleText2), str(cardPartner), str(cardSet)]
        return fetchedData

    def translateData(self, inputData):
        translatedData = []
        currentSet = ''
        for line in inputData:
            lineData, newSet = self._readLine(line, currentSet)
            currentSet = newSet
            if 'card_name' not in lineData:
                continue
            lineDict = {}
            lineDict['card_name'] = lineData['card_name']
            lineDict['card_set'] = lineData['card_set']
            lineDict['card_count'] = lineData['card_count']
            translatedData.append(lineDict)
        return translatedData

    def importCard(self, importData, cursor):
        cardName = importData['card_name']
        cardNameClean = cardName.replace('\'', '\'\'')
        cardSet = importData['card_set']
        cardCount = importData['card_count']
        cardData = ''
        dataCheck = f"SELECT * FROM cards WHERE name = '{cardNameClean}' AND card_set = '{cardSet}'"
        dataCheck = cursor.execute(dataCheck)
        dataCheck = dataCheck.fetchall()
        if cardSet == '' or dataCheck == []:
            cardData = self.fetchCardData(cardName, cardSet, cardCount)
            if cardData is None:
                infoText = f"({cardSet})\t{cardName} - CARD NOT FOUND"
                return infoText
            cardSet = cardData[len(cardData) - 1]
        dataCheck = f"SELECT * FROM cards WHERE name = '{cardNameClean}' COLLATE NOCASE AND card_set = '{cardSet}'"
        dataCheck = cursor.execute(dataCheck)
        dataCheck = dataCheck.fetchall()
        if dataCheck != []:
            info = self.changeCount(dataCheck[0], cardCount, cursor)
            infoText = f"({cardSet})\t{cardName} - Added {info['card_count']} to {info['old_count']}, new total {info['count_difference']}."
            return infoText
        else:
            self.addCard(cardData, cursor)
            sleep(0.05)
            infoText = f"({cardSet})\t{cardName} - Added {cardData[2]} to 0, new total {cardData[2]}. Entry added."
            return infoText

    def addCard(self, cardData, cursor):
        if cardData != '':
            insertData = cardData
            cursor.execute(self.insertStructure, insertData)

    def changeCount(self, cardData, cardCount, cursor):
        cardCount = int(cardCount)
        oldCount = cardData['count']
        newCount = oldCount + cardCount
        countDifference = cardCount + oldCount
        name = cardData['name'].replace('\'', '\'\'')
        if countDifference < 0:
            countDifference = 0
        infoDeletion = ''
        if newCount < 1:
            update = f"""DELETE FROM cards
                WHERE name = '{name}' COLLATE NOCASE AND card_set = '{cardData['card_set']}' COLLATE NOCASE"""
            infoDeletion = ' Entry deleted.'
        else:
            update = f"""UPDATE cards SET count = {newCount}
                WHERE name = '{name}' COLLATE NOCASE AND card_set = '{cardData['card_set']}' COLLATE NOCASE"""
        if cardCount < 0:
            cardCount *= -1
        if cardCount > oldCount:
            cardCount = oldCount
        cursor.execute(update)
        info = {'card_count': cardCount, 'old_count': oldCount, 'count_difference': countDifference,
                        'deletion': infoDeletion, 'card_set': cardData['card_set']}
        return info

    def saveDeck(self, deckName, cardList):
        dataCheck = f"SELECT * FROM decks WHERE deck_name = '{deckName}'"
        dataCheck = self.cursor.execute(dataCheck)
        dataCheck = dataCheck.fetchall()
        if dataCheck == []:
            self.createDeck(deckName)
        update = f"""UPDATE decks SET card_list = '{cardList}'
                WHERE deck_name = '{deckName}'"""
        self.cursor.execute(update)

    def removeCard(self, removeData, minimumCounts, cursor):
        cardName = removeData['card_name']
        cardNameClean = cardName.replace('\'', '\'\'')
        cardSet = removeData['card_set'].upper()
        cardCount = int(removeData['card_count']) * -1
        if cardSet == 'ALL' or cardSet == 'ANY' or cardSet == '':
            dataCheck = f"SELECT * FROM cards WHERE name = '{cardNameClean}' COLLATE NOCASE"
            dataCheck = cursor.execute(dataCheck)
            dataCheck = dataCheck.fetchall()
        else:
            dataCheck = f"SELECT * FROM cards WHERE name = '{cardNameClean}' COLLATE NOCASE AND card_set = '{cardSet}'"
            dataCheck = cursor.execute(dataCheck)
            dataCheck = dataCheck.fetchall()
        if dataCheck == []:
            infoText = []
            infoText.append(f"({cardSet})\t{cardName} - CARD NOT FOUND")
            return infoText
        if cardSet == 'ALL':
            infoText = []
            for entry in dataCheck:
                removeCount = cardCount
                deckInfo = ''
                if entry['card_id'] in minimumCounts:
                    if (entry['count'] + cardCount) < minimumCounts[entry['card_id']]:
                        removeCount = int(entry['count'] - minimumCounts[entry['card_id']]) * -1
                        deckInfo = ' Could not delete all, some of the cards are in decks.'
                info = self.changeCount(entry, removeCount, cursor)
                infoText.append(f"({info['card_set']})\t{cardName} - Removed {info['card_count']} from {info['old_count']}, new total {info['count_difference']}.{info['deletion']}{deckInfo}")
            return infoText
        elif cardSet == 'ANY' or cardSet == '':
            infoText = []
            for entry in dataCheck:
                removeCount = cardCount
                deckInfo = ''
                if entry['card_id'] in minimumCounts:
                    if (entry['count'] + cardCount) < minimumCounts[entry['card_id']]:
                        removeCount = int(entry['count'] - minimumCounts[entry['card_id']]) * -1
                        deckInfo = ' Could not delete all, some of the cards are in decks.'
                info = self.changeCount(entry, removeCount, cursor)
                infoText.append(f"({info['card_set']})\t{cardName} - Removed {info['card_count']} from {info['old_count']}, new total {info['count_difference']}.{info['deletion']}{deckInfo}")
                cardCount += info['old_count']
                if cardCount >= 0:
                    break
            return infoText
        else:
            infoText = []
            entry = dataCheck[0]
            removeCount = cardCount
            deckInfo = ''
            if entry['card_id'] in minimumCounts:
                if (entry['count'] + cardCount) < minimumCounts[entry['card_id']]:
                    removeCount = int(entry['count'] - minimumCounts[entry['card_id']]) * -1
                    deckInfo = ' Could not delete all, some of the cards are in decks.'
            info = self.changeCount(entry, removeCount, cursor)
            infoText.append(f"({cardSet})\t{cardName} - Removed {info['card_count']} from {info['old_count']}, new total {info['count_difference']}.{info['deletion']}{deckInfo}")
            return infoText

    def createDeck(self, deckName):
        if deckName == '':
            return False, "No deck name entered."
        nameCheck = f"SELECT * FROM decks WHERE deck_name = '{deckName}'"
        nameCheck = self.cursor.execute(nameCheck)
        nameCheck = nameCheck.fetchall()
        if nameCheck != []:
            return False, f"{deckName} - deck already exists."
        insertData = [deckName, '']
        insertStructure = """INSERT INTO decks
                        (deck_name, card_list)
                        VALUES (?, ?)"""
        self.cursor.execute(insertStructure, insertData)
        return True, f"{deckName} - deck created."

    def deleteDeck(self, deckName):
        update = f"""DELETE FROM decks
                    WHERE deck_name = '{deckName}'"""
        self.cursor.execute(update)

    def colorConverter(self, color):
        if color != '[]':
            for symbol in self.colorCleanupList:
                color = color.replace(symbol, '')
            color = color.split(',')
            if len(color) > 1:
                color = self.colorDictionary['multi']
            else:
                color = self.colorDictionary[color[0]]
        else:
            color = self.colorDictionary['none']
        return color

    def symbolConverter(self, input, type, size, exact):
        output = input
        symbolSize = size
        cardSymbology = f"SELECT * FROM {type}"
        cardSymbology = self.cursor.execute(cardSymbology)
        cardSymbology = [dict(row) for row in cardSymbology.fetchall()]
        cardSymbolPath = os.path.join(self.path, self.imageDirectory, type)
        for symbol in cardSymbology:
            if '\n' in input:
                output = output.replace('\n', '<p>')
            if symbol['symbol'] in input and not exact or symbol['symbol'] == input:
                symbolPath = os.path.join(cardSymbolPath, f"{symbol['path']}")
                symbolReplace = f"<img src={symbolPath} width={symbolSize} height={symbolSize}>"
                output = output.replace(symbol['symbol'], symbolReplace)
                if exact:
                    return output
        return output

    def _toolTipBase(self, data, bgColor):
        cardName = data['name']
        if data['mana_cost'] != 'None':
            cardCost = f"{self.symbolConverter(data['mana_cost'], 'symbology', 20, False)} "
        else:
            cardCost = ''
        cardType = data['type_line']
        cardOracle = self.symbolConverter(data['oracle_text'], 'symbology', 20, False)
        cardPower = data['power']
        cardToughness = data['toughness']
        if cardPower != 'None' and cardToughness != 'None':
            cardStats = f"<br><tr><td align='left'>{cardPower}/{cardToughness}</td></tr>"
        else:
            cardStats = ''
        if data['layout'] == 'meld':
            cardCombine = f"<tr><td align='left'>Combination: {data['name_2']}</td></tr>"
        else:
            cardCombine = ''
        if data['partner'] != 'None':
            cardPartner = f"<tr><td align='left'>Partner: {data['partner']}</td></tr>"
        else:
            cardPartner = ''
        if data['layout'] in self.doubleFacedCards:
            cardName2 = data['name_2']
            if data['mana_cost_2'] != 'None':
                cardCost2 = f"{self.symbolConverter(data['mana_cost_2'], 'symbology', 20, False)} "
            else:
                cardCost2 = ''
            cardType2 = data['type_line_2']
            cardOracle2 = self.symbolConverter(data['oracle_text_2'], 'symbology', 20, False)
            cardPower2 = data['power_2']
            cardToughness2 = data['toughness_2']
            if cardPower2 != 'None' and cardToughness2 != 'None':
                cardStats2 = f"<br><tr><td align='left'>{cardPower2}/{cardToughness2}</td></tr>"
            else:
                cardStats2 = ''
            secondFaceText = f"""<table width=450 style='margin: 3px; background: {bgColor}; margin-top: 1px;'>
                            <tr><td align='left'>{cardName2}</td><td align='right'>{cardCost2}</td></tr>
                            <tr><td align='left'>{cardType2}<br></td></tr>
                            <tr><td align='left'>{cardOracle2}</td></tr>
                            {cardStats2}
                            </table>"""
        else:
            secondFaceText = ''
        tooltipText = f"""<table width=450 style='margin: 3px; background: {bgColor};'>
                            <tr><td align='left'>{cardName}</td><td align='right'>{cardCost}</td></tr>
                            <tr><td align='left'>{cardType}<br></td></tr>
                            <tr><td align='left'>{cardOracle}</td></tr>
                            {cardStats}
                            {cardPartner}
                            {cardCombine}
                            </table>
                            {secondFaceText}
                            """
        return tooltipText

    def toolTip(self, data, countData):
        bgColor = '#f5f6fa'
        borderColor = self.colorConverter(data['color_identity'])
        toolTipBase = self._toolTipBase(data, bgColor)
        if countData != '':
            setCount = countData[0]
            totalCount = countData[1]
            toolTipExtra = f"Total: {totalCount}<br>"
            for cardSet in setCount:
                if setCount[cardSet]['count'] != 0:
                    toolTipExtra += f"{setCount[cardSet]['count']} {self.symbolConverter(cardSet, 'sets', 30, True)} "
            toolTipExtra = f"""<table width=450 style='margin: 3px; background: {borderColor}; margin-top: 1px;'>
                                <tr><td align='left'>{toolTipExtra} </td></tr></table>"""
        else:
            toolTipExtra = f"""<table width=450 style='margin: 3px; background: {borderColor}; margin-top: 1px;'>
                        <tr><td align='left'>{self.symbolConverter(data['card_set'], 'sets', 30, True)} </td></tr></table>"""
        toolTip = f"""<table width=450 style='background: {borderColor}'>
                        <tr><td>{toolTipBase}{toolTipExtra}</td></tr></table>"""
        return toolTip

    def fetchSymbologyData(self, type):
        symbologyURL = f"https://api.scryfall.com/{type}"
        symbologyResult = urllib.request.urlopen(symbologyURL)
        symbologyData = json.loads(symbologyResult.read().decode())
        data = symbologyData['data']
        return data

    def fetchSymbol(self, symbol, symbolType, typeName, typeURL, symbolPath, cursor):
        dataCheck = f"SELECT * FROM {symbolType} WHERE symbol = '{symbol[typeName].upper()}'"
        dataCheck = cursor.execute(dataCheck)
        dataCheck = dataCheck.fetchall()
        if dataCheck == []:
            if symbolType == 'symbology':
                symbolName = re.compile('.+/(.+).svg').match(symbol['svg_uri']).groups()[0]
                cardSymbolStructure = f"""INSERT INTO {symbolType}
                                        (symbol, path)
                                        VALUES (?, ?)
                                        """
                insertData = [symbol[typeName].upper(), f"{symbolType}_{symbolName}.svg"]
            elif symbolType == 'sets':
                symbolName = symbol['code'].upper()
                cardSymbolStructure = f"""INSERT INTO {symbolType}
                                        (name, date, symbol, path)
                                        VALUES (?, ?, ?, ?)
                                        """
                insertData = [symbol['name'], symbol['released_at'], symbol[typeName].upper(), f"{symbolType}_{symbolName}.svg"]
            symbolLocation = os.path.join(symbolPath, f"{symbolType}_{symbolName}.svg")
            cursor.execute(cardSymbolStructure, insertData)
        else:
            symbolLocation = os.path.join(symbolPath, dataCheck[0]['path'])
        if not os.path.isfile(symbolLocation):
            urllib.request.urlretrieve(symbol[typeURL], symbolLocation)
            sleep(0.05)
